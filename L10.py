#შექმენით ლექსიკონი: {0: 10, 1: 20}. დაამატეთ 2 ახალი ელემენტი და დაბეჭდეთ
#მიღებული ლექსიკონი. (გამოიყენეთ update მეთოდიც). წაშალეთ რომელიმე ელემენტი.

firstdic = {0: 10, 1: 20}
scnddic = {2:30, 3:40}
firstdic.update(scnddic)
print(firstdic)



# დაწერეთ პროგრამა, რომელიც შეაერთებს სამ ლექსიკონს:
# dic1={1:10, 2:20}
# dic2={3:30, 4:40}
# dic3={5:50,6:60}
# შედეგი: {1: 10, 2: 20, 3: 30, 4: 40, 5: 50, 6: 60}

dic1={1:10, 2:20}
dic2={3:30, 4:40}
dic3={5:50,6:60}
dic2.update(dic3)
dic1.update(dic2)
print(dic1)



#დაწერეთ პროგრამა რომელიც შეამოწმებს რომელიმე key (გასაღები) არის თუ არა ლექსიკონში: d =
#{1: 10, 2: 20, 3: 30, 4: 40, 5: 50, 6: 60} და დაბეჭდეთ შესაბამისი შეტობინება. (მითითება:გამოიყენეთ in ოპერატორი).
d ={1: 10, 2: 20, 3: 30, 4: 40, 5: 50, 6: 60}

tofind = int(input("შეიყვანეთ ციფრი მოსაძებნად"))

print(d.get(tofind, 'Not found')) # in ოპერატორის გარეშე

if tofind in d.keys(): #in ოპერატორით
    print(d[tofind])
else:print("Not Found")



#მოცემულია ლექსიკონი d = {'x': 10, 'y': 20, 'z': 30} დაბეჭდეთ თითოეული ელემენტის key და value შემდეგნაირად (მითითება: გამოიყენეთ for ციკლი):
# x -> 10
# y -> 20
# z -> 30

d = {'x': 10, 'y': 20, 'z': 30}

for key,value in d.items():
    print(key,"->",value)



#Task 5 დაწერეთ ალგორითმი, რომელიც შექმნის შემდეგი სახის ლექსიკონს (key არის 1-დან 10-მდე რიცხვები, ხოლო value- მათი კუბები). დაბეჭდეთ მიღებული ლექსიკონი.

nums = 1,2,3,4,5,6,7,8,9,10
square = {}

for i in nums:
    d = square.fromkeys([i],pow(i,3))
    print(d)



#Task 6 შექმენით ცარიელი ლექსიკონი და დაამატეთ ელემენტები ქვემოთ მითითებული გამოსახულების მიხედვით. დაბეჭდეთ აღწერილი ადამიანის სახელი, გვარი, ასაკი და შვილების სახელები.
person = {}

person.update({"firstName": "Jane","lastName": "Doe","hobbies":["sky diving","running","singing"],"age":35,"children":[{"firstName": "Alice","age":6},{"firstName":"Bob","age":8}]})
print(person["firstName"],person["lastName"],person["age"],"Years old. His children are",person["children"][0]["firstName"], "and" ,person["children"][1]["firstName"])


#task 7 გამოიყენეთ სალექციო მასალასთან ერთად ატვირთული morsecode.txt ფაილი,რომელშიც მოცემულია მორზეს ანბანი - თითოეულ ხაზზე წარმოდგენილია
# ლათინური ასო ან სიმბოლო შესაბამისი მორზეს კოდებით, რომელიც ერთმანეთისგან გამოყოფილია Tab-ით - ‘\t’ დაწერეთ პროგრამა,
# რომელშიც მომხარებელს შეაყვანინებთ ნებისმიერ ლათინურ ტექსტს. პროგრამამ შეყვანილი ტექსტი უნდა გადაიყვანოს მორზეს ანბანში და დაბეჭდოს
# შედეგი. შედეგის გამოტანის დროს თითოეული სიმბოლოს მორზეს კოდი ერთმანეთს დააშორეთ space-ით. ხოლო სიტყვებს შორის ჩასვით გამყოფი ხაზი | .
# პროგრამის წერისას გამოიყენეთ dictionary.
def morsecode(text):
    code = open("morsecode.txt","r")
    dict = {}
    textup = text.upper()
    toprint = ""

    for line in code:
        x= line.replace("\t"," ")
        li = x.split()
        dict[li[0]] = li[1:]

    input = "".join(textup)
    print(input)
    # splitedin = input.split()
    # print(splitedin)
    for i in input:
        if i in dict:
            m = "".join(dict[i])
            toprint += m + " "
        else:
            toprint += "| "





    print(toprint)

#     splitted.append(line.split('\n'),line.replace("\t"," "))
#
morsecode(input("შეიყვანეთ წინადადება"))


#Task 8 შექმენით სიმრავლე შემდეგი ელემენტებით: 0, 1, 2, 3, 4. დაამატეთ ნებისმიერ 3 ელემენტისურვილისამებრ.
# წაშალეთ ორი ელემენტი სიმრავლიდან. დაბეჭდეთ სიმრავლის ელემენტები ცალ-ცალკე ხაზზე (გამოიყენეთ for ციკლი).
# დაითვალეთ სიმრავლის ელემენტების რაოდენობა.

my_set = {0,1,2,3,4}
addon = {5,6,7}
my_set.update(addon)
print(my_set)
count = 0
for i in my_set:
    count += 1
    print(i)
print("ელემენტების რაოდენობაა",count)

# Task 9შექმენით ორი სიმრავლე: set1 სიმრავლე ელემენტებით "green”, "blue”; set2 სიმრავლე ელემენტებით
# "blue", "yellow”. იპოვეთ ამ ორი სიმრავლის გაერთიანება, თანაკვეთა, სხვაობა და სიმეტრიული
# სხვაობა (შეასრულეთ დავალება ორი გზით: არსებული მეთოდით (ფუნქციით) და შესაბამისი
# ოპერატორით.)

set1 = {"green", "blue"}
set2 = {"blue","yellow"}
print(set1.union(set2)) #gaertianeba
print(set1.intersection(set2))#tanakveta
print(set1.difference(set2)) #set1-set2
print(set2.difference(set1))#set2-set1
print(set1.symmetric_difference(set2)) # თანაკვეთას გამოკლებული გაერთიანება

# Task 10 დაწერეთ პროგრამა რომელიც იპოვის სიმრავლეში მაქსიმალურ და მინიმალურ მნიშვნელობას და
# დაბეჭდეთ შედეგი (სიმრავლე შეარჩიეთ სურვილისამებრ).

numbs = {213,12,41,22,31,4,1}

print("Max - ",max(numbs),"Min - ",min(numbs))

# Task 11 დაწერეთ პროგრამა, სადაც მომხმარებელს შეყვანინებთ ნებისმიერ სტრიქონს. დაბეჭდეთ
# სტრიქონში გამოყენებული ყველა სიმბოლო გამეორებების გარეშე (გამოიყენეთ set).

x = set(input("შეიყვანეთ სტრიქონი"))
print(x)

#task12 მომხარებელს შეაყვანინეთ 2 სიტყვა. დაბეჭდეთ ამ ორი სიტყვის ყველა საერთო ასო.

first = set(input("შეიყვანეთ I სიტყვა"))
scnd = set(input("შეიყვანეთ II სიტყვა"))

print(first.intersection(scnd))





