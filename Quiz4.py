#თორნიკე ელყანიშვილი ქვიზი 4

# გადმოწერეთ titanic.txt ფაილი, რომელშიც მოცემულია ტიტანიკზე მყოფი 891 მგზავრის
# მონაცემები. მარტივი ვიზუალიზაციისთვის შეგიძლიათ იხილოთ ექსელის ფაილი, თუმცა
# ოპერაციები შეასრულეთ txt ფაილთან. პირველ სტრიქონში მოცემულია ველის
# დასახელებები, ხოლო მეორე სტრიქონიდან - ამ ველების შესაბამისი მნიშვნელობები.
#PassengerId,Survived,Pclass,Name,Sex,Age,SibSp,Parch,Ticket,Fare,Cabin,Embarked

#დავალება1
#დაითვალეთ იმ მგზავრების რაოდენობა, რომლებიც იყვნენ ქალები.
#გამოთვალეთ პროცენტული რაოდენობებიც (მაგ. რამდენი პროცენტი იყო ქალი).
#dav 2 დაითვალეთ იმ მგზავრების რაოდენობა, რომლებიც იყვნენ კაცები.
#გამოთვალეთ პროცენტული რაოდენობებიც (მაგ. რამდენი პროცენტი იყო კაცი).
titanic = [line.split(',') for line in open("titanic.txt", "r")]

male = 0
female = 0
for i in titanic:
    if i[4].__contains__("female"):
        female += 1
    else:
        male += 1
m = male / len(titanic) * 100
f = female / len(titanic) * 100
print(male, m.__round__(1), "% Man", "\n", female, f.__round__(1), "% Woman")

#დავ 3
print("_______________________________Task3_________________________________________")
# დაითვალეთ ქალების რაოდენობა, რომლებიც გადარჩა (ვერ გადარჩა).
# გამოთვალეთ პროცენტული რაოდენობებიც.
survivedf = 0
notsurvivedf = 0
for i in titanic:
    if i[4].__contains__("female"):
        if i[1].__contains__("1"):
            survivedf += 1
        else:
            notsurvivedf += 1

surf = survivedf/(survivedf+notsurvivedf) * 100
notsurf = 100-surf
print(survivedf, surf.__round__(1), "% survived woman", "\n", notsurvivedf, notsurf.__round__(1), "% not survived woman")

#dav 4დაითვალეთ კაცების რაოდენობა, რომლებიც გადარჩა (ვერ გადარჩა).
# გამოთვალეთ პროცენტული რაოდენობებიც.
print("_______________________________Task4_________________________________________")
survivedm = 0
notsurvivedm = 0
for i in titanic:
    if i[4].__contains__("female") == False:
        if i[1].__contains__("1"):
            survivedm += 1
        else:
            notsurvivedm += 1

surm = survivedm/(survivedm+notsurvivedm) * 100
notsurm = 100-surm
print(survivedm, "survived man %", surm.__round__(1), "\n", notsurvivedm, "not survived man %", notsurm.__round__(1))

#dav5
print("_______________________________Task5_________________________________________")
# დაითვალეთ იმ მგზავრების რაოდენობა, რომლებსაც ქონდათ პირველი (მეორე,
# მესამე) კლასის ბილეთები. გამოთვალეთ პროცენტული რაოდენობებიც.
class1 = 0
class2 = 0
class3 = 0
for i in titanic:
    if i[2].__contains__("1"):
        class1 += 1
    elif i[2].__contains__("2"):
        class2 += 1
    elif i[2].__contains__("3"):
        class3 += 1
perclass1 = class1/(class1+class2+class3)*100
perclass2 = class2/(class1+class2+class3)*100
perclass3 = class3/(class1+class2+class3)*100
print(class1, "- First Class", perclass1.__round__(1), "%", "\n", class2, "- Second Class", perclass2.__round__(1), "%", "\n", class3, "- Third Class", perclass3.__round__(1), "%")

#dav6
print("_______________________________Task6_________________________________________")
# დაითვალეთ საშუალო ბილეთის ფასი თითოეული კლასის
# ბილეთისთვის.
first = []
second = []
third = []
for i in titanic:
    if i[2].__contains__("1"):
        first.append(i[9])
    elif i[2].__contains__("2"):
        second.append(i[9])
    elif i[2].__contains__("3"):
        third.append(i[9])

sumfirst = 0
sumsecond = 0
sumthird = 0
for each in first:  #pirveli klasi
    sumfirst += float(each)
sumf = sumfirst/len(first)

for each in second:  #meoreklasi
    sumsecond += float(each)
sums = sumsecond/len(second)

for each in third:
    sumthird += float(each)
sumt = sumthird/len(third)

print(sumf.__round__(1), "$ - First Class Average Price", "\n", sums.__round__(1), "$ - Second Class Average Price", "\n", sumt.__round__(1), "$ - Third Class Average Price")

#dav7
#ააგეთ ერთი dictionary ტიპის ცვლადი, რომელშიც შეინახავთ უკვე
# გამოთვლილ ყველა მონაცემს წყვილების სახით.
print("_______________________________Task7_________________________________________")


dic = {
    "Passengers amount": {
        "Male": 577,
        "Male %": 64.8,
        "Female": 314,
        "Female %": 35.2,
    },
    "Survived": {
        "Male": 109,
        "Male %": 18.9,
        "Female": 233,
        "Female %": 74.2
    },
    "Not survived": {
        "Male": 468,
        "Male %": 81.1,
        "Female": 81,
        "Female %": 25.8
    },
    "Classes": {
        "First Class": 216,
        "First Class %": 24.2,
        "Second Class": 184,
        "Second Class %": 20.7,
        "Third Class": 491,
        "Third Class %": 55.1
    },
    "Average Class Price": {
        "First Class $": 94.2,
        "Second Class $": 20.7,
        "Third Class $": 13.7
    }
}
print(dic)

print("_______________________________Task8_________________________________________")
#Task8
# ფაილიდან წამოიღეთ ნებისმიერი ინფორმაცია, რასაც ჩათვლით
# საჭიროდ და კომენტარის სახით მიუთითეთ რა ტიპის ინფორმაცია წამოიღეთ.
# აღნიშნული ინფორმაცია დაამატეთ ზემოთ მითითებულ dictionary-ში.
cherbourg = 0
queenstown = 0
southampton = 0
#ფაილიდან წამოვიღეთ ჩასხდომის ადგილები.
for i in titanic: #  ვიგებთ რამდენი ადამიანი ჩასხდა კონკრეტულ ნავსადგურში
    if i[11].__contains__("C"):
        cherbourg += 1
    elif i[11].__contains__("Q"):
        queenstown += 1
    elif i[11].__contains__("S"):
        southampton += 1

dic.update({ 
    "Boarding place": {
        "Cherbourg": cherbourg,
        "Queenstown": queenstown,
        "Southampton": southampton
    }
})
print(dic)