import random

# # პირველიდავალება
#
# numbs = [5,11,21,55,32]
# print(sum(numbs))# jami
# print(min(numbs))#min
# print(max(numbs))#max
# print(sum(numbs)/len(numbs))#sashualo
# numbs.append(102)#damateba bolo elementad
# print(numbs)
# numbs.insert(2,205)#mesame elementad damateba
# print(numbs)
# numbs.pop(3) #meotxe elementis washla
# print(numbs)
# numbs.sort()
# print(numbs)
#
# # #მეორე დავალება
# # i = 0
# # listt = []
# # while i < 10:
# #     x = listt.append(input("შეიყვანეთ რიცხვი"))
# #     i += 1
# # print(listt)
#
# #მესამე [ ალფაბეტის მიხედვით უკუღმა გამოტანა ]
#
# fruits = ["Apple","Banana","Watermelon","Banana"]
# fruits.sort(reverse=True)
# print(fruits)
#

# #მეოთხე [ funqciis taski ]
# def lobio(n):
#     mult = 1
#     for i in n:
#         mult *= i
#     print(mult)
#
#
# lobio([2, 1, 95, 55, 41])


# #მეხუთე დავალება [კენტების ამოშლა]
# numbers = [2, 1, 95,44,22,42, 55, 41,44,11,21,55,6,1,2,8,4]
# odd = []
# for i in numbers:
#     if i % 2 == 0:
#         odd.append(i)
# print(odd)

#მეექვსე დავალება [ 10 ით გაზრდა ]

# numbers = [ 1, 2 ,3 , 4, 5]
# n = []
# for i in numbers:
#     n.append(i + 10)
# print(n)

# #მეშვიდე დავალება [ რანდომ ელემენტები ]
# x = []
# for i in range(0,10):
#     ran = random.randint(25,110)
#     x.append(ran)
# print(x)
# print(min(x))

# #მერვე დავალება [ თრუ ორ ფოლს ]
# first  = [ 1 ,5 ,7]
# scnd = [2, 3, 5]
#
# if [x for x in first if x in scnd]:
#     print("True")
# else:
#     print("False")

#მეცხრე დავალება [kenti ricxvebis amoshla ]

# def odd(n):
#     odd = []
#     for i in n:
#         if i % 2 == 0:
#             odd.append(i)
#     print(odd)
#
# odd([2, 1, 95, 44, 22, 42, 55, 41, 44, 11, 21, 55, 6, 1, 2, 8, 4])

#მეათე დავალება ( ფაილიდან ამოკითხვა, სპლიტი )
# f = [line.split(' ') for line in open("data.txt", "r")]
#
# for i in f:
#     print(i[1])

#მეთერთმეტე დავალება [ ფაილები ]]
# x = open("data_numbers.txt","w")
# y = "1"
#
# while y != 0:
#     y = input("შეიყვანეთ ციფრები")
#     x.write(y + "\n")
#     if y == "0":break
#
# x.close()

# y = [line.strip() for line in open('data_numbers.txt',"r")]
#
#
# print(" ".join(y))
#y.close()

#მეთორმეტე დავალება [ მატრიცა ]
# x = []
# y = []
# z = []
#
# for i in range(0,4):
#     a = input("შეიყვანეთ მატრიცის პირველი რიგის ციფრები")
#     x.append(a)
# for i in range(0,4):
#     b = input("შეიყვანეთ მატრიცის მეორე რიგის ციფრები")
#     y.append(b)
# for i in range(0,4):
#     c = input("შეიყვანეთ მატრიცის მესამე რიგის ციფრები")
#     z.append(c)
#
#
# print(" ".join(x))
# print(" ".join(y))
# print(" ".join(z))

#მეცამეტე დავალება [shuffle]
# x = [15,21,44,321,5,21,55,291]
# random.shuffle(x)
# print(x)

#მეთოთხმეტე დავალება [ choice ]
# x = [15,21,44,321,5,21,55,291]
# y= random.choice(x)
# print(y)

#მეთხუთმეტე დავალება [ ჯამი ]
# sum = 0
# x = input("შეიყვანეთ რიცხვი")
# t = list(x)
#
# for i in t:
#     new = int(i)
#     sum += new
# print(sum)


#მეთექვსმეტე დავალება [ ყველაზე ხშირად გამეორებადი ]
# x = [1, 5, 23, 5, 12, 2, 5, 1, 18, 5]
# l = max(x,key=x.count)
# print(l,"იბეჭდება",x.count(l),"-ჯერ")
#
# #მეჩვიდმეტე დავალება [contains]
# extensions = ['txt', 'jpg', 'gif', 'html']
# x = input("შეიყვანეთ ფაილის სახელი")
# k = x.split(".")
# if extensions.__contains__(k[1]):
#     print("yes")
# else:print("no")
#
# #მეთვრამეტე [ სტრიქონები ]
# a = 'python php pascal javascript java c++'
# k = a.split(" ")
# print(max(k,key=len))
#
# #მეცხრამეტე [ მედიანა მოდა საშუალო ]
# x = [15,31,21,33,55,421,22,5,21,23]
# sum = 0
# for i in range(0,10):
#     x.append(input("შეიყვანეთ რიცხვი"))
#
# for i in x:
#     new = int(i)
#     sum += new
#     k = sum/len(x)#საშუალო
# print("საშუალოა",k)# საშუალო
#
# x.sort()
#
# if len(x) % 2 == 0:
#     mediana = print("მედიანაა",((x[4]+x[5])/2)) # მედიანა
# else:print("მედიანაა",x[4])
#
#
# l = max(x,key=x.count)
# print("მოდაა",l) # მოდა
#
#

